/********************************************************************
	Rhapsody	: 9.0.1 
	Login		: Administrator
	Component	: AnimComponent 
	Configuration 	: AnimConfig
	Model Element	: Player
//!	Generated Date	: Fri, 11, Dec 2020  
	File Path	: AnimComponent\AnimConfig\Player.cpp
*********************************************************************/

//## auto_generated
#include <oxf\omthread.h>
//## auto_generated
#include "Player.h"
//## link theJudge
#include "Referee.h"
//## package Default

//## class Player
Player::Player(IOxfActive* theActiveContext) {
    setActiveContext(theActiveContext, false);
    theJudge = NULL;
    initStatechart();
}

Player::~Player() {
    cleanUpRelations();
    cleanUpStatechart();
}

void Player::doHit(int rounds) {
    //#[ operation doHit(int)
    //#]
}

void Player::doReceive(int nrounds) {
    //#[ operation doReceive(int)
    GEN(hit(nrounds));
    //#]
}

void Player::doServe(int rounds) {
    //#[ operation doServe(int)
    // Notify other guy
    // Actually serve
    GEN(hit(rounds));
    //#]
}

Referee* Player::getTheJudge() const {
    return theJudge;
}

void Player::setTheJudge(Referee* p_Referee) {
    theJudge = p_Referee;
}

bool Player::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

void Player::initStatechart() {
    delete rootState;
    rootState = new Player_ROOT(this, NULL);
    rootState->subState = NULL;
    rootState->active = NULL;
    Ready = new Player_Ready(this, rootState, rootState);
    Playing = new Player_Playing(this, rootState);
    noHasBall = new Player_noHasBall(this, Playing, rootState);
    hasBall = new Player_hasBall(this, Playing, rootState);
    concept = this;
}

void Player::cleanUpRelations() {
    if(theJudge != NULL)
        {
            theJudge = NULL;
        }
}

void Player::cleanUpStatechart() {
    delete rootState;
    rootState = NULL;
    delete Ready;
    Ready = NULL;
    delete Playing;
    Playing = NULL;
    delete noHasBall;
    noHasBall = NULL;
    delete hasBall;
    hasBall = NULL;
}

void Player::rootStateEntDef() {
    Ready->entDef();
}

IOxfReactive::TakeEventStatus Player::ReadyTakegetReady() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    Ready->exitState();
    Playing->enterState();
    noHasBall->entDef();
    res = eventConsumed;
    return res;
}

IOxfReactive::TakeEventStatus Player::ReadyTakeserve() {
    OMSETPARAMS(serve);
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    Ready->exitState();
    //#[ transition 4 
    
    doServe(params->rounds);
    //#]
    Playing->enterState();
    hasBall->entDef();
    res = eventConsumed;
    return res;
}

void Player::PlayingEntDef() {
    hasBall->entDef();
}

IOxfReactive::TakeEventStatus Player::PlayingTakegameOver() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    Playing->exitState();
    Ready->entDef();
    res = eventConsumed;
    return res;
}

IOxfReactive::TakeEventStatus Player::noHasBallTakeevReceive() {
    OMSETPARAMS(evReceive);
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    noHasBall->exitState();
    //#[ transition 5 
    
    doReceive(params->rounds);
    //#]
    hasBall->entDef();
    res = eventConsumed;
    return res;
}

IOxfReactive::TakeEventStatus Player::hasBallTakehit() {
    OMSETPARAMS(hit);
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    hasBall->exitState();
    //#[ transition 6 
    
    doHit(params->rounds);
    //#]
    noHasBall->entDef();
    res = eventConsumed;
    return res;
}

//#[ ignore
Player_ROOT::Player_ROOT(Player* c, OMState* p) : OMComponentState(p) {
    concept = c;
}

void Player_ROOT::entDef() {
    enterState();
    concept->rootStateEntDef();
}

Player_Ready::Player_Ready(Player* c, OMState* p, OMState* cmp) : OMLeafState(p, cmp) {
    concept = c;
}

IOxfReactive::TakeEventStatus Player_Ready::handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(serve_Default_id))
        {
            res = concept->ReadyTakeserve();
        }
    else if(IS_EVENT_TYPE_OF(getReady_Default_id))
        {
            res = concept->ReadyTakegetReady();
        }
    
    if(res == eventNotConsumed)
        {
            res = parent->handleEvent();
        }
    return res;
}

Player_Playing::Player_Playing(Player* c, OMState* p) : OMOrState(p) {
    concept = c;
}

void Player_Playing::entDef() {
    enterState();
    concept->PlayingEntDef();
}

IOxfReactive::TakeEventStatus Player_Playing::handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(gameOver_Default_id))
        {
            res = concept->PlayingTakegameOver();
        }
    
    if(res == eventNotConsumed)
        {
            res = parent->handleEvent();
        }
    return res;
}

Player_noHasBall::Player_noHasBall(Player* c, OMState* p, OMState* cmp) : OMLeafState(p, cmp) {
    concept = c;
}

IOxfReactive::TakeEventStatus Player_noHasBall::handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(evReceive_Default_id))
        {
            res = concept->noHasBallTakeevReceive();
        }
    
    if(res == eventNotConsumed)
        {
            res = parent->handleEvent();
        }
    return res;
}

Player_hasBall::Player_hasBall(Player* c, OMState* p, OMState* cmp) : OMLeafState(p, cmp) {
    concept = c;
}

IOxfReactive::TakeEventStatus Player_hasBall::handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(hit_Default_id))
        {
            res = concept->hasBallTakehit();
        }
    
    if(res == eventNotConsumed)
        {
            res = parent->handleEvent();
        }
    return res;
}
//#]

/*********************************************************************
	File Path	: AnimComponent\AnimConfig\Player.cpp
*********************************************************************/
