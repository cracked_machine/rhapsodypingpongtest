/*********************************************************************
	Rhapsody	: 9.0.1 
	Login		: Administrator
	Component	: AnimComponent 
	Configuration 	: AnimConfig
	Model Element	: Pong
//!	Generated Date	: Fri, 11, Dec 2020  
	File Path	: AnimComponent\AnimConfig\Pong.h
*********************************************************************/

#ifndef Pong_H
#define Pong_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include "Default.h"
//## class Pong
#include "Player.h"
//## link itsPing
class Ping;

//## auto_generated
class Referee;

//## package Default

//## class Pong
class Pong : public Player {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    Pong(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~Pong();
    
    ////    Operations    ////
    
    //## operation doHit(int)
    void doHit(int rounds);
    
    ////    Additional operations    ////
    
    //## auto_generated
    Ping* getItsPing() const;
    
    //## auto_generated
    void setItsPing(Ping* p_Ping);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();
    
    ////    Relations and components    ////
    
    Ping* itsPing;		//## link itsPing
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsPing(Ping* p_Ping);
    
    //## auto_generated
    void _setItsPing(Ping* p_Ping);
    
    //## auto_generated
    void _clearItsPing();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus ReadyTakeserve();
    
    ////    Framework    ////
    
//#[ ignore
    Pong* concept;
//#]
};

//#[ ignore
class Pong_rootState : public Player_ROOT {
    ////    Constructors and destructors    ////
    
public :

    //## statechart_method
    Pong_rootState(Pong* c, OMState* p);
    
    ////    Framework    ////
    
    Pong* concept;
};

class Pong_Ready : public Player_Ready {
    ////    Constructors and destructors    ////
    
public :

    //## statechart_method
    Pong_Ready(Pong* c, OMState* p, OMState* cmp);
    
    ////    Framework operations    ////
    
    //## statechart_method
    IOxfReactive::TakeEventStatus handleEvent();
    
    ////    Framework    ////
    
    Pong* concept;
};

class Pong_Playing : public Player_Playing {
    ////    Constructors and destructors    ////
    
public :

    //## statechart_method
    Pong_Playing(Pong* c, OMState* p);
    
    ////    Framework    ////
    
    Pong* concept;
};

class Pong_noHasBall : public Player_noHasBall {
    ////    Constructors and destructors    ////
    
public :

    //## statechart_method
    Pong_noHasBall(Pong* c, OMState* p, OMState* cmp);
    
    ////    Framework    ////
    
    Pong* concept;
};

class Pong_hasBall : public Player_hasBall {
    ////    Constructors and destructors    ////
    
public :

    //## statechart_method
    Pong_hasBall(Pong* c, OMState* p, OMState* cmp);
    
    ////    Framework    ////
    
    Pong* concept;
};
//#]

#endif
/*********************************************************************
	File Path	: AnimComponent\AnimConfig\Pong.h
*********************************************************************/
