/********************************************************************
	Rhapsody	: 9.0.1 
	Login		: Administrator
	Component	: AnimComponent 
	Configuration 	: AnimConfig
	Model Element	: Referee
//!	Generated Date	: Fri, 11, Dec 2020  
	File Path	: AnimComponent\AnimConfig\Referee.cpp
*********************************************************************/

//## auto_generated
#include <oxf\omthread.h>
//## auto_generated
#include "Referee.h"
//## link itsPing
#include "Ping.h"
//## attribute currentServer
#include "Player.h"
//## link itsPong
#include "Pong.h"
//## package Default

//## class Referee
Referee::Referee(IOxfActive* theActiveContext) : currentServer(NULL) {
    setActiveContext(theActiveContext, false);
    initStatechart();
    //#[ operation Referee()
    //#]
}

Referee::~Referee() {
    cleanUpRelations();
    cleanUpStatechart();
}

void Referee::changeCurrentServer() {
    //#[ operation changeCurrentServer()
    if (currentServer== (*getItsPing()))
        currentServer = (*getItsPong());
    else
        currentServer = (*getItsPing());
    //#]
}

void Referee::makePlay() {
    //#[ operation makePlay()
    // Create the players
    addItsPing( new Ping());
    addItsPong(new Pong());
    // Connect them to each other and to yourself
    (*getItsPing())->setItsPong(*getItsPong());
    (*getItsPing())->setTheJudge(this);
    (*getItsPong())->setTheJudge(this);
    // Let them "loose"
    (*getItsPing())->startBehavior();
    (*getItsPong())->startBehavior();
    // Flip a coin to determine who starts the game
    // Currently always ping
    currentServer = *getItsPing();
    GEN(go);
    
    //#]
}

void Referee::startGame() {
    //#[ operation startGame()
    int rounds;
    rounds = 7;
    //cout<<"Enter number of rounds ('0' to quit) ";
    //cin>>rounds;
    if (rounds==0) exit(0);
    // Ping always serves
    currentServer->GEN(serve(rounds));
    changeCurrentServer();
    //#]
}

OMIterator<Ping*> Referee::getItsPing() const {
    OMIterator<Ping*> iter(itsPing);
    return iter;
}

void Referee::addItsPing(Ping* p_Ping) {
    itsPing.add(p_Ping);
}

void Referee::removeItsPing(Ping* p_Ping) {
    itsPing.remove(p_Ping);
}

void Referee::clearItsPing() {
    itsPing.removeAll();
}

OMIterator<Pong*> Referee::getItsPong() const {
    OMIterator<Pong*> iter(itsPong);
    return iter;
}

void Referee::addItsPong(Pong* p_Pong) {
    itsPong.add(p_Pong);
}

void Referee::removeItsPong(Pong* p_Pong) {
    itsPong.remove(p_Pong);
}

void Referee::clearItsPong() {
    itsPong.removeAll();
}

bool Referee::startBehavior() {
    bool done = false;
    done = OMReactive::startBehavior();
    return done;
}

Player * Referee::getCurrentServer() const {
    return currentServer;
}

void Referee::setCurrentServer(Player * p_currentServer) {
    currentServer = p_currentServer;
}

void Referee::initStatechart() {
    delete rootState;
    rootState = new Referee_ROOT(this, NULL);
    rootState->subState = NULL;
    rootState->active = NULL;
    ready = new Referee_ready(this, rootState, rootState);
    Lunch = new Referee_Lunch(this, rootState, rootState);
    game = new Referee_game(this, rootState, rootState);
    concept = this;
}

void Referee::cleanUpRelations() {
    {
        itsPing.removeAll();
    }
    {
        itsPong.removeAll();
    }
}

void Referee::cleanUpStatechart() {
    delete rootState;
    rootState = NULL;
    delete ready;
    ready = NULL;
    delete Lunch;
    Lunch = NULL;
    delete game;
    game = NULL;
}

void Referee::rootStateEntDef() {
    //#[ transition 0 
    makePlay();
    //#]
    ready->entDef();
}

IOxfReactive::TakeEventStatus Referee::readyTakego() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    ready->exitState();
    //#[ transition 1 
    startGame();
    //#]
    game->entDef();
    res = eventConsumed;
    return res;
}

IOxfReactive::TakeEventStatus Referee::LunchTakenotHungry() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    Lunch->exitState();
    ready->entDef();
    res = eventConsumed;
    return res;
}

IOxfReactive::TakeEventStatus Referee::gameTakegameOver() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    game->exitState();
    ready->entDef();
    res = eventConsumed;
    return res;
}

IOxfReactive::TakeEventStatus Referee::gameTakehungry() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    game->exitState();
    Lunch->entDef();
    res = eventConsumed;
    return res;
}

//#[ ignore
Referee_ROOT::Referee_ROOT(Referee* c, OMState* p) : OMComponentState(p) {
    concept = c;
}

void Referee_ROOT::entDef() {
    enterState();
    concept->rootStateEntDef();
}

Referee_ready::Referee_ready(Referee* c, OMState* p, OMState* cmp) : OMLeafState(p, cmp) {
    concept = c;
}

IOxfReactive::TakeEventStatus Referee_ready::handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(go_Default_id))
        {
            res = concept->readyTakego();
        }
    
    if(res == eventNotConsumed)
        {
            res = parent->handleEvent();
        }
    return res;
}

Referee_Lunch::Referee_Lunch(Referee* c, OMState* p, OMState* cmp) : OMLeafState(p, cmp) {
    concept = c;
}

IOxfReactive::TakeEventStatus Referee_Lunch::handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(notHungry_Default_id))
        {
            res = concept->LunchTakenotHungry();
        }
    
    if(res == eventNotConsumed)
        {
            res = parent->handleEvent();
        }
    return res;
}

Referee_game::Referee_game(Referee* c, OMState* p, OMState* cmp) : OMLeafState(p, cmp) {
    concept = c;
}

IOxfReactive::TakeEventStatus Referee_game::handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(gameOver_Default_id))
        {
            res = concept->gameTakegameOver();
        }
    else if(IS_EVENT_TYPE_OF(hungry_Default_id))
        {
            res = concept->gameTakehungry();
        }
    
    if(res == eventNotConsumed)
        {
            res = parent->handleEvent();
        }
    return res;
}
//#]

/*********************************************************************
	File Path	: AnimComponent\AnimConfig\Referee.cpp
*********************************************************************/
