/*********************************************************************
	Rhapsody	: 9.0.1 
	Login		: Administrator
	Component	: AnimComponent 
	Configuration 	: AnimConfig
	Model Element	: Default
//!	Generated Date	: Fri, 11, Dec 2020  
	File Path	: AnimComponent\AnimConfig\Default.h
*********************************************************************/

#ifndef Default_H
#define Default_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include <oxf\event.h>
//## auto_generated
class Ping;

//## auto_generated
class Player;

//## auto_generated
class Pong;

//## auto_generated
class Referee;

//#[ ignore
#define gameOver_Default_id 18601

#define getReady_Default_id 18602

#define serve_Default_id 18603

#define evReceive_Default_id 18604

#define hit_Default_id 18605

#define go_Default_id 18606

#define hungry_Default_id 18607

#define notHungry_Default_id 18608
//#]

//## package Default



//## event gameOver()
class gameOver : public OMEvent {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    gameOver();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

//## event getReady()
class getReady : public OMEvent {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    getReady();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

//## event serve(int)
class serve : public OMEvent {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    serve(int p_rounds);
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
    
    ////    Framework    ////
    
    int rounds;		//## auto_generated
};

//## event evReceive(int)
class evReceive : public OMEvent {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    evReceive(int p_rounds);
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
    
    ////    Framework    ////
    
    int rounds;		//## auto_generated
};

//## event hit(int)
class hit : public OMEvent {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    hit(int p_rounds);
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
    
    ////    Framework    ////
    
    int rounds;		//## auto_generated
};

//## event go()
class go : public OMEvent {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    go();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

//## event hungry()
class hungry : public OMEvent {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    hungry();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

//## event notHungry()
class notHungry : public OMEvent {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    notHungry();
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual bool isTypeOf(const short id) const;
};

#endif
/*********************************************************************
	File Path	: AnimComponent\AnimConfig\Default.h
*********************************************************************/
