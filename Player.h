/*********************************************************************
	Rhapsody	: 9.0.1 
	Login		: Administrator
	Component	: AnimComponent 
	Configuration 	: AnimConfig
	Model Element	: Player
//!	Generated Date	: Fri, 11, Dec 2020  
	File Path	: AnimComponent\AnimConfig\Player.h
*********************************************************************/

#ifndef Player_H
#define Player_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf\omreactive.h>
//## auto_generated
#include <oxf\state.h>
//## auto_generated
#include <oxf\event.h>
//## link theJudge
class Referee;

//## package Default

//## class Player
class Player : public OMReactive {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    Player(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    virtual ~Player();
    
    ////    Operations    ////
    
    // Make noise.
    // Notify neighbour.
    //## operation doHit(int)
    virtual void doHit(int rounds);
    
    //## operation doReceive(int)
    virtual void doReceive(int nrounds);
    
    //## operation doServe(int)
    virtual void doServe(int rounds);
    
    ////    Additional operations    ////
    
    //## auto_generated
    Referee* getTheJudge() const;
    
    //## auto_generated
    void setTheJudge(Referee* p_Referee);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();
    
    //## auto_generated
    void cleanUpStatechart();
    
    ////    Relations and components    ////
    
    Referee* theJudge;		//## link theJudge
    
    ////    Framework operations    ////

public :

    //## statechart_method
    void rootStateEntDef();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus ReadyTakegetReady();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus ReadyTakeserve();
    
    //## statechart_method
    void PlayingEntDef();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus PlayingTakegameOver();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus noHasBallTakeevReceive();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus hasBallTakehit();
    
    ////    Framework    ////
    
//#[ ignore
    Player* concept;
    
    OMState* Ready;
    
    OMState* Playing;
    
    OMState* noHasBall;
    
    OMState* hasBall;
//#]
};

//#[ ignore
class Player_ROOT : public OMComponentState {
    ////    Constructors and destructors    ////
    
public :

    //## statechart_method
    Player_ROOT(Player* c, OMState* p);
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual void entDef();
    
    ////    Framework    ////
    
    Player* concept;
};

class Player_Ready : public OMLeafState {
    ////    Constructors and destructors    ////
    
public :

    //## statechart_method
    Player_Ready(Player* c, OMState* p, OMState* cmp);
    
    ////    Framework operations    ////
    
    //## statechart_method
    IOxfReactive::TakeEventStatus handleEvent();
    
    ////    Framework    ////
    
    Player* concept;
};

class Player_Playing : public OMOrState {
    ////    Constructors and destructors    ////
    
public :

    //## statechart_method
    Player_Playing(Player* c, OMState* p);
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual void entDef();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus handleEvent();
    
    ////    Framework    ////
    
    Player* concept;
};

class Player_noHasBall : public OMLeafState {
    ////    Constructors and destructors    ////
    
public :

    //## statechart_method
    Player_noHasBall(Player* c, OMState* p, OMState* cmp);
    
    ////    Framework operations    ////
    
    //## statechart_method
    IOxfReactive::TakeEventStatus handleEvent();
    
    ////    Framework    ////
    
    Player* concept;
};

class Player_hasBall : public OMLeafState {
    ////    Constructors and destructors    ////
    
public :

    //## statechart_method
    Player_hasBall(Player* c, OMState* p, OMState* cmp);
    
    ////    Framework operations    ////
    
    //## statechart_method
    IOxfReactive::TakeEventStatus handleEvent();
    
    ////    Framework    ////
    
    Player* concept;
};
//#]

#endif
/*********************************************************************
	File Path	: AnimComponent\AnimConfig\Player.h
*********************************************************************/
