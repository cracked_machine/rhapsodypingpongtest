/********************************************************************
	Rhapsody	: 9.0.1 
	Login		: Administrator
	Component	: AnimComponent 
	Configuration 	: AnimConfig
	Model Element	: Pong
//!	Generated Date	: Fri, 11, Dec 2020  
	File Path	: AnimComponent\AnimConfig\Pong.cpp
*********************************************************************/

//## auto_generated
#include <oxf\omthread.h>
//## auto_generated
#include "Pong.h"
//## link itsPing
#include "Ping.h"
//## auto_generated
#include "Referee.h"
//## package Default

//## class Pong
Pong::Pong(IOxfActive* theActiveContext) {
    setActiveContext(theActiveContext, false);
    itsPing = NULL;
    initStatechart();
}

Pong::~Pong() {
    cleanUpRelations();
}

void Pong::doHit(int rounds) {
    //#[ operation doHit(int)
    if (rounds==0) {
        GEN(gameOver());
        getItsPing()->GEN(gameOver());
        theJudge->GEN(gameOver());
    } else {
        omcout<<"\nPong\n"<<omflush;
        getItsPing()->GEN(evReceive(rounds-1));
    }
    //#]
}

Ping* Pong::getItsPing() const {
    return itsPing;
}

void Pong::setItsPing(Ping* p_Ping) {
    if(p_Ping != NULL)
        {
            p_Ping->_setItsPong(this);
        }
    _setItsPing(p_Ping);
}

bool Pong::startBehavior() {
    bool done = false;
    done = Player::startBehavior();
    return done;
}

void Pong::initStatechart() {
    rootState->subState = NULL;
    rootState->active = NULL;
    delete Ready;
    Ready = new Pong_Ready(this, rootState, rootState);
    concept = this;
}

void Pong::cleanUpRelations() {
    if(itsPing != NULL)
        {
            Pong* p_Pong = itsPing->getItsPong();
            if(p_Pong != NULL)
                {
                    itsPing->__setItsPong(NULL);
                }
            itsPing = NULL;
        }
}

void Pong::__setItsPing(Ping* p_Ping) {
    itsPing = p_Ping;
}

void Pong::_setItsPing(Ping* p_Ping) {
    if(itsPing != NULL)
        {
            itsPing->__setItsPong(NULL);
        }
    __setItsPing(p_Ping);
}

void Pong::_clearItsPing() {
    itsPing = NULL;
}

IOxfReactive::TakeEventStatus Pong::ReadyTakeserve() {
    OMSETPARAMS(serve);
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    Ready->exitState();
    //#[ transition 4 
    
    getItsPing()->GEN(getReady);
    doServe(params->rounds);
    //#]
    Playing->enterState();
    hasBall->entDef();
    res = eventConsumed;
    return res;
}

//#[ ignore
Pong_rootState::Pong_rootState(Pong* c, OMState* p) : Player_ROOT(c, p) {
    concept = c;
}

Pong_Ready::Pong_Ready(Pong* c, OMState* p, OMState* cmp) : Player_Ready(c, p, cmp) {
    concept = c;
}

IOxfReactive::TakeEventStatus Pong_Ready::handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(serve_Default_id))
        {
            res = concept->ReadyTakeserve();
        }
    else if(IS_EVENT_TYPE_OF(getReady_Default_id))
        {
            res = concept->ReadyTakegetReady();
        }
    
    if(res == eventNotConsumed)
        {
            res = parent->handleEvent();
        }
    return res;
}

Pong_Playing::Pong_Playing(Pong* c, OMState* p) : Player_Playing(c, p) {
    concept = c;
}

Pong_noHasBall::Pong_noHasBall(Pong* c, OMState* p, OMState* cmp) : Player_noHasBall(c, p, cmp) {
    concept = c;
}

Pong_hasBall::Pong_hasBall(Pong* c, OMState* p, OMState* cmp) : Player_hasBall(c, p, cmp) {
    concept = c;
}
//#]

/*********************************************************************
	File Path	: AnimComponent\AnimConfig\Pong.cpp
*********************************************************************/
