/********************************************************************
	Rhapsody	: 9.0.1 
	Login		: Administrator
	Component	: AnimComponent 
	Configuration 	: AnimConfig
	Model Element	: Default
//!	Generated Date	: Fri, 11, Dec 2020  
	File Path	: AnimComponent\AnimConfig\Default.cpp
*********************************************************************/

//## auto_generated
#include "Default.h"
//## auto_generated
#include "Ping.h"
//## auto_generated
#include "Player.h"
//## auto_generated
#include "Pong.h"
//## auto_generated
#include "Referee.h"
//## package Default



//## event gameOver()
gameOver::gameOver() {
    setId(gameOver_Default_id);
}

bool gameOver::isTypeOf(const short id) const {
    return (gameOver_Default_id==id);
}

//## event getReady()
getReady::getReady() {
    setId(getReady_Default_id);
}

bool getReady::isTypeOf(const short id) const {
    return (getReady_Default_id==id);
}

//## event serve(int)
serve::serve(int p_rounds) : rounds(p_rounds) {
    setId(serve_Default_id);
}

bool serve::isTypeOf(const short id) const {
    return (serve_Default_id==id);
}

//## event evReceive(int)
evReceive::evReceive(int p_rounds) : rounds(p_rounds) {
    setId(evReceive_Default_id);
}

bool evReceive::isTypeOf(const short id) const {
    return (evReceive_Default_id==id);
}

//## event hit(int)
hit::hit(int p_rounds) : rounds(p_rounds) {
    setId(hit_Default_id);
}

bool hit::isTypeOf(const short id) const {
    return (hit_Default_id==id);
}

//## event go()
go::go() {
    setId(go_Default_id);
}

bool go::isTypeOf(const short id) const {
    return (go_Default_id==id);
}

//## event hungry()
hungry::hungry() {
    setId(hungry_Default_id);
}

bool hungry::isTypeOf(const short id) const {
    return (hungry_Default_id==id);
}

//## event notHungry()
notHungry::notHungry() {
    setId(notHungry_Default_id);
}

bool notHungry::isTypeOf(const short id) const {
    return (notHungry_Default_id==id);
}

/*********************************************************************
	File Path	: AnimComponent\AnimConfig\Default.cpp
*********************************************************************/
