/*********************************************************************
	Rhapsody	: 9.0.1 
	Login		: Administrator
	Component	: AnimComponent 
	Configuration 	: AnimConfig
	Model Element	: Ping
//!	Generated Date	: Fri, 11, Dec 2020  
	File Path	: AnimComponent\AnimConfig\Ping.h
*********************************************************************/

#ifndef Ping_H
#define Ping_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include "Default.h"
//## class Ping
#include "Player.h"
//## link itsPong
class Pong;

//## auto_generated
class Referee;

//## package Default

//## class Ping
class Ping : public Player {
    ////    Constructors and destructors    ////
    
public :

    //## auto_generated
    Ping(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~Ping();
    
    ////    Operations    ////
    
    //## operation doHit(int)
    void doHit(int rounds);
    
    ////    Additional operations    ////
    
    //## auto_generated
    Pong* getItsPong() const;
    
    //## auto_generated
    void setItsPong(Pong* p_Pong);
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();
    
    ////    Relations and components    ////
    
    Pong* itsPong;		//## link itsPong
    
    ////    Framework operations    ////

public :

    //## auto_generated
    void __setItsPong(Pong* p_Pong);
    
    //## auto_generated
    void _setItsPong(Pong* p_Pong);
    
    //## auto_generated
    void _clearItsPong();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus ReadyTakeserve();
    
    ////    Framework    ////
    
//#[ ignore
    Ping* concept;
//#]
};

//#[ ignore
class Ping_rootState : public Player_ROOT {
    ////    Constructors and destructors    ////
    
public :

    //## statechart_method
    Ping_rootState(Ping* c, OMState* p);
    
    ////    Framework    ////
    
    Ping* concept;
};

class Ping_Ready : public Player_Ready {
    ////    Constructors and destructors    ////
    
public :

    //## statechart_method
    Ping_Ready(Ping* c, OMState* p, OMState* cmp);
    
    ////    Framework operations    ////
    
    //## statechart_method
    IOxfReactive::TakeEventStatus handleEvent();
    
    ////    Framework    ////
    
    Ping* concept;
};

class Ping_Playing : public Player_Playing {
    ////    Constructors and destructors    ////
    
public :

    //## statechart_method
    Ping_Playing(Ping* c, OMState* p);
    
    ////    Framework    ////
    
    Ping* concept;
};

class Ping_noHasBall : public Player_noHasBall {
    ////    Constructors and destructors    ////
    
public :

    //## statechart_method
    Ping_noHasBall(Ping* c, OMState* p, OMState* cmp);
    
    ////    Framework    ////
    
    Ping* concept;
};

class Ping_hasBall : public Player_hasBall {
    ////    Constructors and destructors    ////
    
public :

    //## statechart_method
    Ping_hasBall(Ping* c, OMState* p, OMState* cmp);
    
    ////    Framework    ////
    
    Ping* concept;
};
//#]

#endif
/*********************************************************************
	File Path	: AnimComponent\AnimConfig\Ping.h
*********************************************************************/
