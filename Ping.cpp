/********************************************************************
	Rhapsody	: 9.0.1 
	Login		: Administrator
	Component	: AnimComponent 
	Configuration 	: AnimConfig
	Model Element	: Ping
//!	Generated Date	: Fri, 11, Dec 2020  
	File Path	: AnimComponent\AnimConfig\Ping.cpp
*********************************************************************/

//## auto_generated
#include <oxf\omthread.h>
//## auto_generated
#include "Ping.h"
//## link itsPong
#include "Pong.h"
//## auto_generated
#include "Referee.h"
//## package Default

//## class Ping
Ping::Ping(IOxfActive* theActiveContext) {
    setActiveContext(theActiveContext, false);
    itsPong = NULL;
    initStatechart();
}

Ping::~Ping() {
    cleanUpRelations();
}

void Ping::doHit(int rounds) {
    //#[ operation doHit(int)
    if (rounds==0) {
        GEN(gameOver());
        getItsPong()->GEN(gameOver());
        theJudge->GEN(gameOver());
    } else {
        omcout<<"\nPing\n"<< omflush;
        getItsPong()->GEN(evReceive(rounds-1));
    }
    //#]
}

Pong* Ping::getItsPong() const {
    return itsPong;
}

void Ping::setItsPong(Pong* p_Pong) {
    if(p_Pong != NULL)
        {
            p_Pong->_setItsPing(this);
        }
    _setItsPong(p_Pong);
}

bool Ping::startBehavior() {
    bool done = false;
    done = Player::startBehavior();
    return done;
}

void Ping::initStatechart() {
    rootState->subState = NULL;
    rootState->active = NULL;
    delete Ready;
    Ready = new Ping_Ready(this, rootState, rootState);
    concept = this;
}

void Ping::cleanUpRelations() {
    if(itsPong != NULL)
        {
            Ping* p_Ping = itsPong->getItsPing();
            if(p_Ping != NULL)
                {
                    itsPong->__setItsPing(NULL);
                }
            itsPong = NULL;
        }
}

void Ping::__setItsPong(Pong* p_Pong) {
    itsPong = p_Pong;
}

void Ping::_setItsPong(Pong* p_Pong) {
    if(itsPong != NULL)
        {
            itsPong->__setItsPing(NULL);
        }
    __setItsPong(p_Pong);
}

void Ping::_clearItsPong() {
    itsPong = NULL;
}

IOxfReactive::TakeEventStatus Ping::ReadyTakeserve() {
    OMSETPARAMS(serve);
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    Ready->exitState();
    //#[ transition 4 
    
    getItsPong()->GEN(getReady());
    doServe(params->rounds);
    //#]
    Playing->enterState();
    hasBall->entDef();
    res = eventConsumed;
    return res;
}

//#[ ignore
Ping_rootState::Ping_rootState(Ping* c, OMState* p) : Player_ROOT(c, p) {
    concept = c;
}

Ping_Ready::Ping_Ready(Ping* c, OMState* p, OMState* cmp) : Player_Ready(c, p, cmp) {
    concept = c;
}

IOxfReactive::TakeEventStatus Ping_Ready::handleEvent() {
    IOxfReactive::TakeEventStatus res = eventNotConsumed;
    if(IS_EVENT_TYPE_OF(serve_Default_id))
        {
            res = concept->ReadyTakeserve();
        }
    else if(IS_EVENT_TYPE_OF(getReady_Default_id))
        {
            res = concept->ReadyTakegetReady();
        }
    
    if(res == eventNotConsumed)
        {
            res = parent->handleEvent();
        }
    return res;
}

Ping_Playing::Ping_Playing(Ping* c, OMState* p) : Player_Playing(c, p) {
    concept = c;
}

Ping_noHasBall::Ping_noHasBall(Ping* c, OMState* p, OMState* cmp) : Player_noHasBall(c, p, cmp) {
    concept = c;
}

Ping_hasBall::Ping_hasBall(Ping* c, OMState* p, OMState* cmp) : Player_hasBall(c, p, cmp) {
    concept = c;
}
//#]

/*********************************************************************
	File Path	: AnimComponent\AnimConfig\Ping.cpp
*********************************************************************/
