/*********************************************************************
	Rhapsody	: 9.0.1 
	Login		: Administrator
	Component	: AnimComponent 
	Configuration 	: AnimConfig
	Model Element	: Referee
//!	Generated Date	: Fri, 11, Dec 2020  
	File Path	: AnimComponent\AnimConfig\Referee.h
*********************************************************************/

#ifndef Referee_H
#define Referee_H

//## auto_generated
#include <oxf\oxf.h>
//## auto_generated
#include "Default.h"
//## auto_generated
#include <oxf\omreactive.h>
//## auto_generated
#include <oxf\state.h>
//## auto_generated
#include <oxf\event.h>
//## auto_generated
#include <oxf\omcollec.h>
//## link itsPing
class Ping;

//## attribute currentServer
class Player;

//## link itsPong
class Pong;

//## package Default

//## class Referee
class Referee : public OMReactive {
    ////    Constructors and destructors    ////
    
public :

    //## operation Referee()
    Referee(IOxfActive* theActiveContext = 0);
    
    //## auto_generated
    ~Referee();
    
    ////    Operations    ////
    
    //## operation changeCurrentServer()
    void changeCurrentServer();
    
    //## operation makePlay()
    void makePlay();
    
    //## operation startGame()
    void startGame();
    
    ////    Additional operations    ////
    
    //## auto_generated
    OMIterator<Ping*> getItsPing() const;
    
    //## auto_generated
    void addItsPing(Ping* p_Ping);
    
    //## auto_generated
    void removeItsPing(Ping* p_Ping);
    
    //## auto_generated
    void clearItsPing();
    
    //## auto_generated
    OMIterator<Pong*> getItsPong() const;
    
    //## auto_generated
    void addItsPong(Pong* p_Pong);
    
    //## auto_generated
    void removeItsPong(Pong* p_Pong);
    
    //## auto_generated
    void clearItsPong();
    
    //## auto_generated
    virtual bool startBehavior();

protected :

    //## auto_generated
    Player * getCurrentServer() const;
    
    //## auto_generated
    void setCurrentServer(Player * p_currentServer);
    
    //## auto_generated
    void initStatechart();
    
    //## auto_generated
    void cleanUpRelations();
    
    //## auto_generated
    void cleanUpStatechart();
    
    ////    Attributes    ////
    
    Player * currentServer;		//## attribute currentServer
    
    ////    Relations and components    ////
    
    OMCollection<Ping*> itsPing;		//## link itsPing
    
    OMCollection<Pong*> itsPong;		//## link itsPong
    
    ////    Framework operations    ////

public :

    //## statechart_method
    void rootStateEntDef();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus readyTakego();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus LunchTakenotHungry();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus gameTakegameOver();
    
    //## statechart_method
    IOxfReactive::TakeEventStatus gameTakehungry();
    
    ////    Framework    ////
    
//#[ ignore
    Referee* concept;
    
    OMState* ready;
    
    OMState* Lunch;
    
    OMState* game;
//#]
};

//#[ ignore
class Referee_ROOT : public OMComponentState {
    ////    Constructors and destructors    ////
    
public :

    //## statechart_method
    Referee_ROOT(Referee* c, OMState* p);
    
    ////    Framework operations    ////
    
    //## statechart_method
    virtual void entDef();
    
    ////    Framework    ////
    
    Referee* concept;
};

class Referee_ready : public OMLeafState {
    ////    Constructors and destructors    ////
    
public :

    //## statechart_method
    Referee_ready(Referee* c, OMState* p, OMState* cmp);
    
    ////    Framework operations    ////
    
    //## statechart_method
    IOxfReactive::TakeEventStatus handleEvent();
    
    ////    Framework    ////
    
    Referee* concept;
};

class Referee_Lunch : public OMLeafState {
    ////    Constructors and destructors    ////
    
public :

    //## statechart_method
    Referee_Lunch(Referee* c, OMState* p, OMState* cmp);
    
    ////    Framework operations    ////
    
    //## statechart_method
    IOxfReactive::TakeEventStatus handleEvent();
    
    ////    Framework    ////
    
    Referee* concept;
};

class Referee_game : public OMLeafState {
    ////    Constructors and destructors    ////
    
public :

    //## statechart_method
    Referee_game(Referee* c, OMState* p, OMState* cmp);
    
    ////    Framework operations    ////
    
    //## statechart_method
    IOxfReactive::TakeEventStatus handleEvent();
    
    ////    Framework    ////
    
    Referee* concept;
};
//#]

#endif
/*********************************************************************
	File Path	: AnimComponent\AnimConfig\Referee.h
*********************************************************************/
