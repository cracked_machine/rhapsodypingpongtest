/********************************************************************
	Rhapsody	: 9.0.1 
	Login		: Administrator
	Component	: AnimComponent 
	Configuration 	: AnimConfig
	Model Element	: AnimConfig
//!	Generated Date	: Fri, 11, Dec 2020  
	File Path	: AnimComponent\AnimConfig\MainAnimComponent.cpp
*********************************************************************/

//## auto_generated
#include "MainAnimComponent.h"
//## auto_generated
#include "Referee.h"
int main(int argc, char* argv[]) {
    int status = 0;
    if(OXF::initialize())
        {
            Referee * p_Referee;
            p_Referee = new Referee;
            p_Referee->startBehavior();
            //#[ configuration AnimComponent::AnimConfig 
            //#]
            OXF::start();
            delete p_Referee;
            status = 0;
        }
    else
        {
            status = 1;
        }
    return status;
}

/*********************************************************************
	File Path	: AnimComponent\AnimConfig\MainAnimComponent.cpp
*********************************************************************/
